package com.san;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringbootCamelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCamelApplication.class, args);
	}
	
	@RequestMapping("/")
	public String sayHi() {
		return "Hi Sankar! Welcome to Spring Boot - Docker - OpenShift";
	}
	
	@RequestMapping("/greet")
	public String greet() {
		return "Greetings Sankar! Sample application for Spring Boot - Docker - OpenShift";
	}
}
