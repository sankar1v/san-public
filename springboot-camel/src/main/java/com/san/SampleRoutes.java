package com.san;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;

@Slf4j
@Configuration
public class SampleRoutes extends RouteBuilder {

	@Value("${echo.value}") String echoStringValue;
	
	public String getEchoStringValue() {
		return echoStringValue;
	}

	public void setEchoStringValue(String echoStringValue) {
		this.echoStringValue = echoStringValue;
	}

	@Override
	public void configure() throws Exception {
		from("timer://foo?fixedRate=true&period=20000") // 20 seconds
			.to("direct:audit");
		
		from("direct:audit").setBody(constant("HELLO WORLD")).convertBodyTo(String.class)
			.to("direct:logme");
		
		from("direct:logme")
			.to("log:com.san.SampleRoutes");
	}

}
